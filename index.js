// Set up the dependencies
const express = require('express');
const mongoose = require('mongoose');
// This allows us to use all the routes define in 'taskRoute.js'
const taskRoutes = require('./routes/taskRoutes')

// Server set-up
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Database connection
// Connecting to MongoDB Atlas
mongoose.set('strictQuery', false)
mongoose.connect('mongodb+srv://admin123:admin123@cluster0.8gw8qty.mongodb.net/s36?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', console.error.bind(console, 'Connection Error'))
db.once('open', () => console.log("We're connected to the cloud database"))

/////////////////////////////////////////////


// Add the task route
// Allows all the task routes created in the 'taskRoutes.js' file to use '/tasks' route
app.use('/tasks', taskRoutes)

app.listen(port, () => console.log (`Now listening to port ${port}`))