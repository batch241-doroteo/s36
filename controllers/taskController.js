// Controllers contains the functions and business logic of our Express JS application
// Meaning all the operations it can do will be placed in this file

// Uses the 'require' directive to allow access to the 'Task' model which allows us to access methods to performs CRUD operations.
// Allows us to use the contents of the 'task.js' in the models folder
const Task = require('../models/task')

// Controller function for GETTING ALL THE TASKS
// Defines the functions to be used in the 'taskRoutes.js' file and export these functions
module.exports.getAllTasks = () => {

	// the '.then' method is used to wait for the Mongoose 'find' method to finish before sending the result back to the route and eventually to the client/Postman
	return Task.find({}).then(result => {
		return result;
	})
}

module.exports.createTask = (requestBody) => {
	
	// Create a task object based on the Mongoose Model "task"
	let newTask = new Task({
		// Sets the 'name' property with the value received from the Postman/client
		name: requestBody.name
	})
	// the 1st parameter will store the result return by the Mongoose save method
	// the 2nd parameter will store the 'error' object
	return newTask.save().then((task, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			return task
		}
	})
}

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
		if(error) {
			console.log(error)
			return false
		} else {
			return removedTask
		}
	})
}

module.exports.updateTask = (taskId, reqBody) => {

	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return false
		} 
		result.name = reqBody.name;
		return result.save().then((updatedTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr)
			} else {
				return updatedTask
			}
		})
	})
}

////////////////////////////////////////

module.exports.getOneTasks = (taskId, reqBody) => {

	return Task.findById(taskId).then(result => {
		return result;
	})
}

module.exports.updateOneTask = (taskId, reqBody) => {

	return Task.findById(taskId).then((result, error) => {
		if(error) {
			console.log(error)
			return false
		} 
		result.status = reqBody.status;
		return result.save().then((updatedTask, saveErr) => {
			if(saveErr) {
				console.log(saveErr)
			} else {
				return updatedTask
			}
		})
	})
}